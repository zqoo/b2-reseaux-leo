# ***B2 Réseau 2022 - TP1***
## TP1 - Mise en jambes

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

- nom, adresse MAC et adresse IP de l'interface WiFi
```
theorivry@Theos-MacBook-Pro ~ % ipconfig

en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether f4:5c:89:8d:a2:a9 
	inet6 fe80::8ab:4046:f239:9e52%en0 prefixlen 64 secured scopeid 0x4 
	inet 10.33.19.108 netmask 0xfffffc00 broadcast 10.33.19.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active

```

**🌞 Affichez votre gateway**

- utilisez une commande pour connaître l'adresse IP de la [passerelle](../../cours/lexique.md#passerelle-ou-gateway) de votre carte WiFi
```
theorivry@Theos-MacBook-Pro ~ % route get default | grep gateway
    gateway: 10.33.19.254
```
  
### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

- trouvez l'IP, la MAC et la [gateway](../../cours/lexique.md#passerelle-ou-gateway) pour l'interface WiFi de votre PC
>Petite pomme > System Preferences > Network > Wi-Fi > Advanced > TCP/IP
![](https://i.imgur.com/umiFb76.png)
>Petite pomme > System Preferences > Network > Wi-Fi > Advanced > Hardware
![](https://i.imgur.com/au95PeE.png)



### Questions

- 🌞 à quoi sert la [gateway](../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

```
Permet de relier deux réseaux informatiques aux caractéristiques différentes
```
## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

- changez l'adresse IP de votre carte WiFi pour une autre
- ne changez que le dernier octet
  - par exemple pour `10.33.1.10`, ne changez que le `10`
  - valeur entre 1 et 254 compris
  
 ```
theorivry@Theos-MacBook-Pro ~ % ifconfig | grep inet
	inet 10.33.19.222 netmask 0xfffffc00 broadcast 10.33.19.255
```

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

```
Si l'adresse IP est deja utilisée, nous ne pouvons pas nous connecter
```

---

# II. Exploration locale en duo

## 1. Prérequis

- deux PCs avec ports RJ45
- un câble RJ45
- **firewalls désactivés** sur les deux PCs

## 2. Câblage

Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. **Bap.**

## Création du réseau (oupa)

## 3. Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
  - choisissez une IP qui commence par "192.168"
  - utilisez un /30 (que deux IP disponibles)
- vérifiez à l'aide de commandes que vos changements ont pris effet

```
PS C:\Users\theo>  ipconfig

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b5fd:54e4:4129:c6b7%4
    -> Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.0.1

```
- utilisez `ping` pour tester la connectivité entre les deux machines

```
PS C:\Users\theo>  ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=64

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```
- affichez et consultez votre table ARP

```
PS C:\Users\theo> arp -a

Interface : 192.168.0.2 --- 0x4
  Adresse Internet      Adresse physique      Type
   -> 192.168.0.1           08-97-98-ab-f1-58     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

## 4. Utilisation d'un des deux comme gateway

Ca, ça peut toujours dépann. Comme pour donner internet à une tour sans WiFi quand y'a un PC portable à côté, par exemple. 

L'idée est la suivante :

- vos PCs ont deux cartes avec des adresses IP actuellement
  - la carte WiFi, elle permet notamment d'aller sur internet, grâce au réseau YNOV
  - la carte Ethernet, qui permet actuellement de joindre votre coéquipier, grâce au réseau que vous avez créé :)
- si on fait un tit schéma tout moche, ça donne ça :

```schema
  Internet           Internet
     |                   |
    WiFi                WiFi
     |                   |
    PC 1 ---Ethernet--- PC 2
    
- internet joignable en direct par le PC 1
- internet joignable en direct par le PC 2
```

- vous allez désactiver Internet sur une des deux machines, et vous servir de l'autre machine pour accéder à internet.

```schema
  Internet           Internet
     X                   |
     X                  WiFi
     |                   |
    PC 1 ---Ethernet--- PC 2
    
- internet joignable en direct par le PC 2
- internet joignable par le PC 1, en passant par le PC 2
```
---

- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu
  - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait
- 🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
ciremy@ciremy-Aspire-A315-56:~/work/ynov/tp linux/tp-linux$ ping 192.168.137.1
PING 192.168.137.1 (192.168.137.1) 56(84) bytes of data.
64 bytes from 192.168.137.1: icmp_seq=1 ttl=128 time=2.44 ms
```

## 5. Petit chat privé

Sur un Windows, ça donne un truc comme ça :

```schema
C:\Users\It4\Desktop\netcat-win32-1.11>nc.exe -h
[v1.11 NT www.vulnwatch.org/netcat/]
connect to somewhere:   nc [-options] hostname port[s] [ports] ...
listen for inbound:     nc -l -p port [options] [hostname] [port]
options:
        -d              detach from console, background mode

        -e prog         inbound program to exec [dangerous!!]
        -g gateway      source-routing hop point[s], up to 8
        -G num          source-routing pointer: 4, 8, 12, ...
        -h              this cruft
        -i secs         delay interval for lines sent, ports scanned
        -l              listen mode, for inbound connects
        -L              listen harder, re-listen on socket close
        -n              numeric-only IP addresses, no DNS
        -o file         hex dump of traffic
        -p port         local port number
        -r              randomize local and remote ports
        -s addr         local source address
        -t              answer TELNET negotiation
        -u              UDP mode
        -v              verbose [use twice to be more verbose]
        -w secs         timeout for connects and final net reads
        -z              zero-I/O mode [used for scanning]
port numbers can be individual or ranges: m-n [inclusive]
```

L'idée ici est la suivante :

- l'un de vous jouera le rôle d'un *serveur*
- l'autre sera le *client* qui se connecte au *serveur*


- 🌞 **sur le PC *serveur*** avec par exemple l'IP 192.168.1.1
  - `nc.exe -l -p 8888`
    - "`netcat`, écoute sur le port numéro 8888 stp"
  - il se passe rien ? Normal, faut attendre qu'un client se connecte

```
S C:\Users\theo\Dropbox\PC\Downloads\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 8888
```


- 🌞 **sur le PC *client*** avec par exemple l'IP 192.168.1.2
  - `nc.exe 192.168.1.1 8888`
    - "`netcat`, connecte toi au port 8888 de la machine 192.168.1.1 stp"
  - une fois fait, vous pouvez taper des messages dans les deux sens

```
S C:\Users\theo\Dropbox\PC\Downloads\netcat-win32-1.11\netcat-1.11> .\nc.exe 192.168.137.1
test
hello
```

---

- 🌞 pour aller un peu plus loin
  - le serveur peut préciser sur quelle IP écouter, et ne pas répondre sur les autres
  - par exemple, on écoute sur l'interface Ethernet, mais pas sur la WiFI
  - pour ce faire `nc.exe -l -p PORT_NUMBER IP_ADDRESS`
  - par exemple `nc.exe -l -p 9999 192.168.1.37`
  - on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur `127.0.0.1`

```
PS C:\Users\theo\Dropbox\PC\Downloads\netcat-win32-1.11\netcat-1.11> ./nc.exe -l -p 9999 192.168.137.2
Test Serveur
Test Client
```

## 6. Firewall

Toujours par 2.

Le but est de configurer votre firewall plutôt que de le désactiver

- Activez votre firewall
- 🌞 Autoriser les `ping`
  - configurer le firewall de votre OS pour accepter le `ping`
  - aidez vous d'internet
  - on rentrera dans l'explication dans un prochain cours mais sachez que `ping` envoie un message *ICMP de type 8* (demande d'ECHO) et reçoit un message *ICMP de type 0* (réponse d'écho) en retour

![](https://i.imgur.com/PtYWm84.png)

- 🌞 Autoriser le traffic sur le port qu'utilise `nc`
  - on parle bien d'ouverture de **port** TCP et/ou UDP
  - on ne parle **PAS** d'autoriser le programme `nc`
  - choisissez arbitrairement un port entre 1024 et 20000

![](https://i.imgur.com/f7GqDU3.png)

  - vous utiliserez ce port pour [communiquer avec `netcat`](#5-petit-chat-privé-) par groupe de 2 toujours
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne


On réactive les firewalls :

PS C:\Users\theo\Dropbox\PC\Downloads\netcat-win32-1.11\netcat-1.11> ./nc.exe -l -p 9999 192.168.137.2
Test Serveur
test Client
# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

- l'IP qu'on vous a donné
- le réseau dans lequel cette IP est valable

🌞Exploration du DHCP, depuis votre PC

- afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
```
theorivry@Theos-MacBook-Pro ~ % ipconfig getpacket en0
server_identifier (ip): 10.33.19.254
```
- cette adresse a une durée de vie limitée. C'est le principe du ***bail DHCP*** (ou *DHCP lease*). Trouver la date d'expiration de votre bail DHCP
```
theorivry@Theos-MacBook-Pro ~ % ipconfig getpacket en0
lease_time (uint32): 0x27cfa
```

## 2. DNS

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur
```
theorivry@Theos-MacBook-Pro ~ % ipconfig getpacket en0
domain_name_server (ip_mult): {8.8.8.8, 8.8.4.4, 1.1.1.1}
```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

  - faites un *lookup* (*lookup* = "dis moi à quelle IP se trouve tel nom de domaine")
    - pour `google.com`
    ```
    theorivry@Theos-MacBook-Pro ~ % nslookup
    > google.com
    Non-authoritative answer:
    Name:	google.com
    Address: 216.58.215.46
    ```
    - pour `ynov.com`
    ```
    theorivry@Theos-MacBook-Pro ~ % nslookup
    > ynov.com
    Non-authoritative answer:
    Name:	ynov.com
    Address: 104.26.10.233
    Name:	ynov.com
    Address: 104.26.11.233
    Name:	ynov.com
    Address: 172.67.74.226
    ```
    - interpréter les résultats de ces commandes
    ```
    Ynov possède trois serveurs DNS contrairement a google
    ```
  - déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
  - faites un *reverse lookup* (= "dis moi si tu connais un nom de domaine pour telle IP")
    - pour l'adresse `78.74.21.21`
    ```
    theorivry@Theos-MacBook-Pro ~ % nslookup 78.74.21.21
    Server:		8.8.8.8
    Address:	8.8.8.8#53

    Non-authoritative answer:
    21.21.74.78.in-addr.arpa	name = host-78-74-21-21.homerun.telia.com.
    ```
    - pour l'adresse `92.146.54.88`
    ```
    theorivry@Theos-MacBook-Pro ~ % nslookup 92.146.54.88
    Server:		8.8.8.8
    Address:	8.8.8.8#53

    ** server can't find 88.54.146.92.in-addr.arpa: NXDOMAIN
    ```
    - interpréter les résultats
    ```
    La première adresse est celle de Telia.com
    La seconde n'existe pas
    ```

# IV. Wireshark

Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.

Il peut :

- enregistrer le trafic réseau, pour l'analyser plus tard
- afficher le trafic réseau en temps réel

**On peut TOUT voir.**

Un peu austère aux premiers abords, une manipulation très basique permet d'avoir une très bonne compréhension de ce qu'il se passe réellement.

- téléchargez l'outil [Wireshark](https://www.wireshark.org/)
- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un `ping` entre vous et la passerelle

![](https://i.imgur.com/cIHY3Ie.png)

  - un `netcat` entre vous et votre mate, branché en RJ45


![](https://i.imgur.com/znfBqw2.png)

  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
  - prenez moi des screens des trames en question
  - on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe